import logo from "./logo.svg";
import "./App.css";
import { withAuthenticator } from "aws-amplify-react";
import Auth from "@aws-amplify/auth";
import Todos from "./Todos";

function App(props) {
  if (props.authState !== "signedIn") {
    return null;
  }

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <Todos />
        <span onClick={() => Auth.signOut()}>Logout ME</span>
      </header>
    </div>
  );
}

export default withAuthenticator(App);

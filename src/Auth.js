import { useState } from "react";
import { Auth } from "aws-amplify";

function ErrorMessage({ message }) {
  if (message) {
    return <div style={{ color: "red" }}>{message}</div>;
  }

  return null;
}

function SignIn({ onSignIn }) {
  return (
    <form id="signInForm" method="POST" onSubmit={e => e.preventDefault()}>
      <label htmlFor="username">User Name:</label>&nbsp;
      <input type="text" id="username" />
      <br />
      <label htmlFor="password">Password:</label>&nbsp;
      <input type="password" id="password" />
      <br />
      <button onClick={onSignIn}>Log In</button>
    </form>
  );
}

function CodeConfirm({ onConfirm }) {
  return (
    <form id="codeConfirmForm" method="POST" onSubmit={e => e.preventDefault()}>
      <label htmlFor="confirmationCode">Confirmation code from SMS:</label>
      &nbsp;
      <input type="text" id="confirmationCode" />
      <br />
      <button onClick={onConfirm}>Check the Code</button>
    </form>
  );
}

function AuthAWS(props) {
  const [user, setUser] = useState(null);
  const [errorMessage, setErrorMessage] = useState(null);
  console.log(props);

  async function signIn() {
    try {
      setErrorMessage(null);

      const username = document.getElementById("username").value;
      const password = document.getElementById("password").value;
      const user = await Auth.signIn(username, password);

      console.log("User Data: ", user);

      setUser(user);

      if (
        user.challengeName === "SMS_MFA" ||
        user.challengeName === "SOFTWARE_TOKEN_MFA"
      ) {
        props.onStateChange("confirmSignIn");
      }
    } catch (error) {
      console.log("error signing in", error);
      setErrorMessage(error.message);
    }
  }

  async function confirmCode() {
    try {
      setErrorMessage(null);

      const code = document.getElementById("confirmationCode").value;
      const loggedUser = await Auth.confirmSignIn(
        user, // Return object from Auth.signIn()
        code
      );

      console.log("Logged User Data: ", loggedUser);

      setUser(loggedUser);
      props.onStateChange("signedIn");
    } catch (error) {
      // go back to login page
      if (error.code === "NotAuthorizedException") {
        props.onStateChange("signIn");
        return;
      }

      console.log("error signing in", error);
      setErrorMessage(error.message);
    }
  }

  if (props.authState === "signIn") {
    return (
      <>
        <ErrorMessage message={errorMessage} />
        <SignIn onSignIn={signIn} />
      </>
    );
  }

  if (props.authState === "confirmSignIn") {
    return (
      <>
        <ErrorMessage message={errorMessage} />
        <CodeConfirm onConfirm={confirmCode} />
      </>
    );
  }

  return null;
}

export default AuthAWS;

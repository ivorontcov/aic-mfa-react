export const AMPLIFY_CONFIG = {
  aws_project_region: "us-east-1",
  aws_cognito_identity_pool_id:
    "us-east-1:56c4b216-b2e8-4463-bab9-5a7c8163e3bd",
  aws_cognito_region: "us-east-1",
  aws_user_pools_id: "us-east-1_ODVV2fgGM",
  aws_user_pools_web_client_id: "7dhdr3p1k6prokvh0pcfhpkoj",
  oauth: {},
  aws_cognito_login_mechanism: ["PREFERRED_USERNAME"],
  aws_cognito_signup_attributes: ["EMAIL"],
  aws_cognito_mfa_configuration: "OPTIONAL",
  aws_cognito_mfa_types: ["SMS"],
  aws_cognito_password_protection_settings: {
    passwordPolicyMinLength: 8,
    passwordPolicyCharacters: []
  },
  aws_appsync_graphqlEndpoint:
    "https://zqtqtqx2grb6zp2eshitmuydom.appsync-api.us-east-1.amazonaws.com/graphql",
  aws_appsync_region: "us-east-1",
  aws_appsync_authenticationType: "AMAZON_COGNITO_USER_POOLS"
  // "Auth": {
  //   "region": process.env.REACT_APP_AMPLIFY_REGION,
  //   "userPoolId": process.env.REACT_APP_AMPLIFY_USER_POOL_ID,
  //   "userPoolWebClientId": process.env.REACT_APP_AMPLIFY_APP_CLIENT_ID,
  // },
  // "aws_appsync_graphqlEndpoint": process.env.REACT_APP_AMPLIFY_APP_GRAPHQL_ENDPOINT,
  // "aws_appsync_region": process.env.REACT_APP_AMPLIFY_REGION,
  // "aws_appsync_authenticationType": process.env.REACT_APP_AMPLIFY_APP_GRAPHQL_AUTHENTICATION_TYPE,
  // "aws_appsync_apiKey": process.env.REACT_APP_AMPLIFY_APP_GRAPHQL_API_KEY,
};

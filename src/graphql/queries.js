export const listTodos = /* GraphQL */ `
  query listTodos {
    listTodos {
      items {
        id
        description
        name
      }
    }
  }
`;

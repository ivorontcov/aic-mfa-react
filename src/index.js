import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import Auth from "./Auth";
import reportWebVitals from "./reportWebVitals";
import Amplify from "aws-amplify";
import { Authenticator } from "aws-amplify-react";
import { AMPLIFY_CONFIG } from "./config";
Amplify.configure(AMPLIFY_CONFIG);

ReactDOM.render(
  <React.StrictMode>
    <Authenticator hideDefault={true}>
      <App />
      <Auth />
    </Authenticator>
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
